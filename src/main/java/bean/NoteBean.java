package bean;

import dto.NoteDto;
import entity.NoteEntity;
import entity.UserEntity;
import interfaces.NoteBeanInterface;
import interfaces.NoteBeanInterfaceR;
import util.Util;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static util.Util.*;

@Stateless
@Local(NoteBeanInterface.class)
@Remote(NoteBeanInterfaceR.class)
public class NoteBean implements NoteBeanInterface, NoteBeanInterfaceR {

    @PersistenceContext(unitName = "myDataBase")
    EntityManager entityManager;

    @Override
    public NoteEntity create(NoteEntity noteEntity) {
        entityManager.persist(noteEntity);
        return noteEntity;
    }

    @Override
    public NoteDto create(NoteDto noteDto) {
        NoteEntity note = new NoteEntity(noteDto.getText());
        UserEntity user = findUserEntityByUsername(noteDto.getUserDto().getUsername());
        note.setUser(user);
        user.setNotes(note);
        return noteEntityToNoteDto(create(note));
    }

    @Override
    public NoteDto findNoteDto(String noteId) {
        return noteEntityToNoteDto(findNoteEntity(noteId));
    }

    @Override
    public NoteEntity findNoteEntity(String noteId) {
        String query = "SELECT note FROM " + NOTE_TABLE + " note";
        return entityManager.createQuery(query, NoteEntity.class).getResultList().stream()
                .filter(note -> note.getNoteid().equals(noteId))
                .findFirst()
                .orElse(null);
    }

    @Override
    public NoteDto remove(NoteDto noteDto) {
        NoteEntity note = findNoteEntity(noteDto.getNoteid());
        return noteEntityToNoteDto(remove(note));
    }

    @Override
    public NoteEntity remove(NoteEntity noteEntity) {
        NoteEntity note = findNoteEntity(noteEntity.getNoteid());
        String query = "Delete from " + NOTE_TABLE + " where id = " + note.getId();
        entityManager.createQuery(query).executeUpdate();
        return note;
    }

    private UserEntity findUserEntityByUsername(String username) {
        String query = "SELECT user FROM " + USER_TABLE + " user";
        return entityManager.createQuery(query, UserEntity.class)
                .getResultList()
                .stream().filter(user -> user.getUsername().equals(username))
                .findFirst()
                .orElse(null);
    }
}
