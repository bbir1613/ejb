package bean;

import dto.UserDto;
import entity.UserEntity;
import interfaces.UserBeanInterface;
import interfaces.UserBeanInterfaceR;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static util.Util.USER_TABLE;
import static util.Util.userEntityToUserDto;

@Stateless
@Local(UserBeanInterface.class)
@Remote(UserBeanInterfaceR.class)
public class UserBean implements UserBeanInterface, UserBeanInterfaceR {

    @PersistenceContext(unitName = "myDataBase")
    EntityManager entityManager;

    @Override
    public UserEntity create(UserEntity userEntity) {
        entityManager.persist(userEntity);
        return findUserEntityByUsername(userEntity.getUsername());
    }

    @Override
    public UserEntity findUserEntityByUsername(String username) {
        String query = "SELECT user FROM " + USER_TABLE + " user";
        return entityManager.createQuery(query, UserEntity.class)
                .getResultList()
                .stream().filter(user -> user.getUsername().equals(username))
                .findFirst()
                .orElse(null);
    }

    @Override
    public UserDto create(UserDto userDto) {
        UserEntity user = new UserEntity(userDto.getUsername(), userDto.getPassword());
        user = create(user);
        return userEntityToUserDto(user);
    }

    @Override
    public UserDto findUserDto(String username) {
        return userEntityToUserDto(findUserEntityByUsername(username));
    }

}
