package util;

import dto.NoteDto;
import dto.UserDto;
import entity.NoteEntity;
import entity.UserEntity;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

public class Util {
    //region TABLES

    public static final String USER_TABLE = UserEntity.class.getSimpleName();

    public static final String NOTE_TABLE = NoteEntity.class.getSimpleName();

    //endregion
    //region SESSION

    public static final String USER_SESSION_TAG = "user";

    //endregion

    //region URL
    private static final String SERVER_URL = "http://localhost:8080/ejb";

    public static final String LOGIN_PATH = "/login";

    public static final String LOGOUT_PATH = "/logout";


    public static final String ADD_NOTE_PATH = "/add";

    public static final String REMOVE_NOTE_PATH = "/remove";

    //endregion

    //region REQUEST PARAMETERS
    public static final String USERNAME_REQUEST_PARAMETER = "username";

    public static final String PASSWORD_REQUEST_PARAMETER = "password";

    public static final String NOTE_TEXT_REQUEST_PARAMETER = "note_text";

    public static final String NOTE_ID_REQUEST_PARAMETER = "note_id";


    //endregion
    //region HTML

    public static final String LOGIN_PAGE = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>Login</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div style=\"text-align: center;\">\n" +
            "    <form method = \"POST\" action=\"" + SERVER_URL + LOGIN_PATH + "\">\n" +
            "        <p>Username <input type=\"text\" name=\"" + USERNAME_REQUEST_PARAMETER + "\">\n" +
            "        <p>Password <input type=\"text\" name=\"" + PASSWORD_REQUEST_PARAMETER + "\">\n" +
            "        <p><input type=\"submit\" value=\"login\">\n" +
            "     </form>\n" +
            "</div>\n" +
            "</body>\n" +
            "</html>";

    public static final String INVALID_USERNAME_PASSWORD_PAGE = LOGIN_PAGE + "<center><font color =\"red\">INVALID Username OR Password</font></center>";

    public static final String ADD_NOTE_HTML_PAGE = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>Add note</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div style=\"text-align: center;\">\n" +
            "    <form method = \"POST\" action=\"" + SERVER_URL + ADD_NOTE_PATH + "\">\n" +
            "        <p>Text <input type=\"text\" name=\"" + NOTE_TEXT_REQUEST_PARAMETER + "\">\n" +
            "        <p><input type=\"submit\" value=\"add\">\n" +
            "     </form>\n" +
            "</div>\n" +
            "</body>\n" +
            "</html>";

    private static String noteToForm(NoteEntity note) {
        return "<form method = \"POST\" action=\"" + SERVER_URL + REMOVE_NOTE_PATH + "\">\n" +
                "   <p> <input type =\"hidden\" name =\"" + NOTE_ID_REQUEST_PARAMETER + "\" value= \"" + note.getNoteid() + "\" \n" +
                "    <p>" + note.getText() + "<input type=\"submit\" value=\"delete\">\n" +
                "</form>\n";
    }

    public static String userNotesToHtml(Collection<NoteEntity> notes) {
        if (notes == null)
            return "";
        StringBuilder sb = new StringBuilder();
        sb.append("<center>");
        notes.forEach(n -> sb.append(Util.noteToForm(n)));
        sb.append("</center>");
        return sb.toString();
    }

    //endregion

    //region UTIL METHODS
    public static <T> T getSessionAttribute(String tag, HttpSession session, Class<T> clazz) {
        Object obj = session.getAttribute(tag);
        if (obj != null && clazz.isInstance(obj)) {
            return clazz.cast(obj);
        }
        return null;
    }

    public static void Log(String tag, Object... objs) {
        if (objs != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(tag).append(" : ");
            for (Object obj : objs) {
                sb.append(obj.toString()).append(" \t");
            }
            System.out.printf("\"%-40s%s%n\"", tag, sb.toString());
        }
    }

    public static void htmlPageToClient(HttpServletResponse res, String html_page) throws IOException {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        out.println(html_page);
        out.flush();
        out.close();
    }

    public static UserDto userEntityToUserDto(UserEntity userEntity) {
        if (userEntity != null)
            return new UserDto(userEntity.getUsername(), userEntity.getPassword(), userEntity.getUser_id());
        return null;
    }

    public static NoteDto noteEntityToNoteDto(NoteEntity noteEntity) {
        if (noteEntity != null)
            return new NoteDto(noteEntity.getNoteid(), noteEntity.getText(), userEntityToUserDto(noteEntity.getUser()));
        return null;
    }

    //endregion
}
