package interfaces;
import entity.UserEntity;

public interface UserBeanInterface {

    UserEntity create(UserEntity userEntity);

    UserEntity findUserEntityByUsername(String username);

}
