package interfaces;

import entity.NoteEntity;

public interface NoteBeanInterface {

    NoteEntity create(NoteEntity noteEntity);

    NoteEntity findNoteEntity(String noteId);

    NoteEntity remove(NoteEntity noteEntity);
}
