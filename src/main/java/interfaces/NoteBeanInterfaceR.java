package interfaces;

import dto.NoteDto;

public interface NoteBeanInterfaceR {

    NoteDto create(NoteDto noteDto);

    NoteDto findNoteDto(String noteId);

    NoteDto remove(NoteDto noteDto);


}
