package dto;

import java.io.Serializable;

public class NoteDto implements Serializable {

    Long id;
    String noteid;
    String text;
    UserDto userDto;

    public NoteDto() {
    }

    public NoteDto(String text) {
        this.text = text;
    }

    public NoteDto(String noteid, String text) {
        this(text);
        this.noteid = noteid;
    }

    public NoteDto(String noteid, String text, UserDto userDto) {
        this(noteid, text);
        this.userDto = userDto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNoteid() {
        return noteid;
    }

    public void setNoteid(String noteid) {
        this.noteid = noteid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    @Override
    public String toString() {
        return "NoteDto{" +
                "id=" + id +
                ", noteid='" + noteid + '\'' +
                ", text='" + text + '\'' +
                ", userDto=" + userDto +
                '}';
    }
}
