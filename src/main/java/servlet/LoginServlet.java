package servlet;

import entity.UserEntity;
import interfaces.UserBeanInterface;
import util.Util;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static util.Util.*;

@WebServlet(LOGIN_PATH)
public class LoginServlet extends HttpServlet {
    @EJB
    UserBeanInterface userBean;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserEntity user = Util.getSessionAttribute(USER_SESSION_TAG, req.getSession(true), UserEntity.class);
        if (user == null) {
            Util.htmlPageToClient(resp, Util.LOGIN_PAGE);
        } else {
            Util.Log(LoginServlet.class.getCanonicalName(), "ALREADY LOG IN");
            req.getSession(true).setAttribute(USER_SESSION_TAG, user);
            Util.htmlPageToClient(resp, ADD_NOTE_HTML_PAGE + Util.userNotesToHtml(user.getNotes()));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter(Util.USERNAME_REQUEST_PARAMETER);
        String password = req.getParameter(Util.PASSWORD_REQUEST_PARAMETER);
        Util.Log(LoginServlet.class.getCanonicalName(), username, password);
        UserEntity user = userBean.findUserEntityByUsername(username);
        if (user.getPassword().equals(password)) {
            Util.Log(LoginServlet.class.getCanonicalName(), "LOGIN");
            req.getSession(true).setAttribute(USER_SESSION_TAG, user);
            Util.htmlPageToClient(resp, ADD_NOTE_HTML_PAGE + Util.userNotesToHtml(user.getNotes()));
        } else {
            Util.Log(LoginServlet.class.getCanonicalName(), "WRONG");
            Util.htmlPageToClient(resp, INVALID_USERNAME_PASSWORD_PAGE);
        }
    }
}
