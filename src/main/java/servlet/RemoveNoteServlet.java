package servlet;

import bean.NoteBean;
import entity.NoteEntity;
import entity.UserEntity;
import interfaces.NoteBeanInterface;
import util.Util;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Objects;

import static util.Util.*;

@WebServlet(REMOVE_NOTE_PATH)
public class RemoveNoteServlet extends HttpServlet {
    @EJB
    NoteBeanInterface noteBean;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserEntity user = Util.getSessionAttribute(USER_SESSION_TAG, req.getSession(), UserEntity.class);
        assert user != null;
        NoteEntity note = getRequestNoteEntity(req);
        if (note != null) {
            Util.Log(RemoveNoteServlet.class.getCanonicalName(), note.toString());
            note.setUser(user);
            final NoteEntity noteEntity = noteBean.remove(note);
            Collection<NoteEntity> notes = user.getNotes();
            Util.Log(RemoveNoteServlet.class.getCanonicalName(), note.toString());
            notes.removeIf(n-> Objects.equals(n.getId(), noteEntity.getId()));
            user.setNotes(notes);
            req.getSession().setAttribute(USER_SESSION_TAG, user);
        }
        Util.htmlPageToClient(resp, ADD_NOTE_HTML_PAGE + Util.userNotesToHtml(user.getNotes()));
    }

    NoteEntity getRequestNoteEntity(HttpServletRequest req) {
        String noteId = req.getParameter(NOTE_ID_REQUEST_PARAMETER);
        if (noteId == null || noteId.length() == 0) {
            Util.Log(RemoveNoteServlet.class.getCanonicalName(), "Return null");
            return null;
        }
        NoteEntity note = new NoteEntity();
        note.setNoteid(noteId);
        return note;
    }

}
