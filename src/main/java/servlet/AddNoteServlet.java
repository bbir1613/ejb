package servlet;

import entity.NoteEntity;
import entity.UserEntity;
import interfaces.NoteBeanInterface;
import util.Util;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static util.Util.*;

@WebServlet(ADD_NOTE_PATH)
public class AddNoteServlet extends HttpServlet {

    @EJB
    NoteBeanInterface noteBean;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserEntity user = Util.getSessionAttribute(USER_SESSION_TAG, req.getSession(), UserEntity.class);
        assert user != null;
        NoteEntity note = getRequestNoteEntity(req);
        if (note != null) {
            note.setUser(user);
            noteBean.create(note);
            user.setNotes(note);
            req.getSession().setAttribute(USER_SESSION_TAG, user);
        }
        Util.htmlPageToClient(resp, ADD_NOTE_HTML_PAGE + Util.userNotesToHtml(user.getNotes()));
    }

    NoteEntity getRequestNoteEntity(HttpServletRequest req) {
        String noteText = req.getParameter(NOTE_TEXT_REQUEST_PARAMETER);
        if (noteText == null || noteText.length() == 0) {
            return null;
        }
        return new NoteEntity(noteText);
    }
}
