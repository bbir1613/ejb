package servlet;

import entity.UserEntity;
import util.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static util.Util.LOGIN_PAGE;
import static util.Util.LOGOUT_PATH;
import static util.Util.USER_SESSION_TAG;

@WebServlet(LOGOUT_PATH)
public class LogoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserEntity user = Util.getSessionAttribute(USER_SESSION_TAG, req.getSession(true), UserEntity.class);
        if (user != null) {
            req.getSession().setAttribute(USER_SESSION_TAG, null);
        }
        Util.htmlPageToClient(resp, LOGIN_PAGE);
    }
}
