package servlet;

import entity.NoteEntity;
import entity.UserEntity;
import interfaces.NoteBeanInterface;
import interfaces.UserBeanInterface;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

@WebServlet("/pop")
public class PopulateDataBaseServlet extends HttpServlet {
    @EJB
    UserBeanInterface userBean;
    @EJB
    NoteBeanInterface noteBean;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserEntity userEntity = new UserEntity("a", "b");
        userBean.create(userEntity);
        userEntity = new UserEntity("b", "b");
        userBean.create(userEntity);
        userEntity = new UserEntity("c", "b");
        userBean.create(userEntity);
        userEntity = new UserEntity("d", "b");
        userEntity = userBean.create(userEntity);

        NoteEntity noteEntity = new NoteEntity("note");
        noteEntity.setUser(userEntity);
        NoteEntity noteEntity1 = noteBean.create(noteEntity);
        Collection<NoteEntity> notes = userEntity.getNotes();
        notes.add(noteEntity1);
        userEntity.setNotes(notes);

        noteEntity = new NoteEntity("note2");
        noteEntity.setUser(userEntity);
        noteEntity1 = noteBean.create(noteEntity);
        notes.add(noteEntity1);
        userEntity.setNotes(notes);

        noteEntity = new NoteEntity("note3");
        noteEntity.setUser(userEntity);
        noteEntity1 = noteBean.create(noteEntity);
        notes.add(noteEntity1);
        userEntity.setNotes(notes);

        noteEntity = new NoteEntity("note4");
        noteEntity.setUser(userEntity);
        noteEntity1 = noteBean.create(noteEntity);
        notes.add(noteEntity1);
        userEntity.setNotes(notes);

        noteEntity = new NoteEntity("note5");
        noteEntity.setUser(userEntity);
        noteEntity1 = noteBean.create(noteEntity);
        notes.add(noteEntity1);
        userEntity.setNotes(notes);

        final NoteEntity noteEntity2 = noteEntity1;
        noteEntity1 = userEntity.getNotes().stream().filter(n -> n.getId() == noteEntity2.getId()).findFirst().orElse(null);
        noteBean.remove(noteEntity1);


        PrintWriter out = resp.getWriter();
        out.println("Added entities");
        out.flush();
        out.close();
    }
}
