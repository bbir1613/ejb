package entity;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class NoteEntity {

    Long id;
    String noteid;
    String text;
    UserEntity user;

    public NoteEntity(){}

    public NoteEntity(String text){
        this.text = text;
        noteid =UUID.randomUUID().toString();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNoteid() {
        return noteid;
    }

    public void setNoteid(String noteid) {
        this.noteid = noteid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "NoteEntity{" +
                "id=" + id +
                ", noteid='" + noteid + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
